#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>

//WiFiEventHandler wifiConnectHandler;
//WiFiEventHandler wifiDisconnectHandler;

int codigoMaquina = -1;

bool conectar(const char* nomeRede, const char* senha) {
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  WiFi.begin(nomeRede, senha);

  unsigned long espera = 5000;
  unsigned long tempo = millis();
  while (WiFi.status() != WL_CONNECTED && (millis() - tempo < espera)) {
    delay(1000);
  }

  if (isConectado()) {
    WiFi.setAutoReconnect(true);
    WiFi.persistent(true);
    return true;
  }

  return false;
}

boolean isConectado() {
  return WiFi.status() == WL_CONNECTED;
}

String criarMensagemJsonStatus() {
  StaticJsonDocument<20> doc;
  doc["codigo"] = codigoMaquina;

  String dados = "";
  serializeJson(doc, dados);
  return dados;
}

int enviarJsonPOST(String dados, String serverName) {
  int resposta = 0;
  if (isConectado()) {
    WiFiClient cliente;
    HTTPClient http;

    // Your Domain name with URL path or IP address with path
    http.begin(cliente, serverName);

    http.addHeader("Content-Type", "application/json");
    resposta = http.POST(dados);

    // Free resources
    http.end();
  }
  return resposta;
}

String enviarJsonGet(const char* url) {
  String dados = "";

  if (isConectado()) {
    WiFiClient cliente;
    HTTPClient http;    

    http.begin(cliente, url);

    // Send HTTP GET request
    int codigoResposta = http.GET();

    if (codigoResposta == 200) {
      dados = http.getString();
    }
    // Free resources
    http.end();
  }
  return dados;
}

String buscarDataHora() {
  const char* endereco = "http://teste.k08.com.br/api/maquina/datetime/";
  String dataHora = enviarJsonGet(endereco);

  if (dataHora.length() == 0) {
    return "";
  }

  StaticJsonDocument<64> doc;
  deserializeJson(doc, dataHora);
  JsonObject obj = doc.as<JsonObject>();

  String dH = obj["data"].as<String>(); //formato: 10/09/2022 21:44:55
  return dH;
}

String buscarComandaPorId(String idComanda){
  String url = "http://teste.k08.com.br/api/v2/comanda/processa/";
  url.concat(idComanda);
  const char* endereco = url.c_str();
  return enviarJsonGet(endereco);
}

//void onWifiConnect(const WiFiEventStationModeGotIP& event) {
//}
//
//void onWifiDisconnect(const WiFiEventStationModeDisconnected& event) {
//}

void setCodigoMaquina() {
  while (true) {
    if (Serial.available()) {
      codigoMaquina = Serial.parseInt();
      break;
    }
  }
}

void buscarRedesDisponiveis() {
  int n = WiFi.scanNetworks();

  if (n == 0) {
    Serial.print("");
    return;
  }

  for (int j = 0; j < n; j++) {
    Serial.print(WiFi.SSID(j)+"\r\n");
  }
}

void setup() {
  Serial.begin(9600);

  //Register event handlers
//  wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
//  wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);  
}

void loop() {
  if (Serial.available()) {
    char codigo = Serial.read();

    switch (codigo) { //.write nao aparece no monitor serial
      case 'A': {
          //Serial.println("caso A");
          enviarJsonPOST(criarMensagemJsonStatus(), "http://teste.k08.com.br/api/maquina/sit/");
          break;
        }
      case 'B': {
          //Serial.println("caso B");
          String dados = Serial.readString();
          int retorno = enviarJsonPOST(dados, "http://teste.k08.com.br/api/comanda/adiciona/");
          Serial.write(retorno == 200?'T':'W');
          break;
        }
      case 'C': {
          //Serial.println("caso C");
          Serial.write(isConectado() ? 'T' : 'W');
          break;
        }
      case 'D': {
          //Serial.println("caso D");
          Serial.print(buscarDataHora());
          break;
        }
      case 'E': {
          //Serial.println("caso E");
          setCodigoMaquina();
          break;
        }
      case 'F': {
          //Serial.println("caso F");
          String nomeSenha = Serial.readString();
          int idx = nomeSenha.indexOf('|');          
          String redeNome = nomeSenha.substring(0,idx);
          String redeSenha = nomeSenha.substring(idx+1,nomeSenha.length());
          boolean conectado = conectar(redeNome.c_str(), redeSenha.c_str());
          Serial.write(conectado?'T':'W');
          Serial.flush();
          break;
        }
      case 'G': {
        buscarRedesDisponiveis();
        break;
      }
      case 'H': {
        WiFi.disconnect();
        break;
      }
    case 'J': {
        String idComanda = Serial.readString();
    Serial.print(buscarComandaPorId(idComanda));
        break;
      }
      default:
        break;
    }
  }
}
